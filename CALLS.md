## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Akamai_property_manager. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Akamai_property_manager.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>

  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapHasAdapterEntity(entityType, entityId, callback)</td>
    <td style="padding:15px">This call verifies the adapter has the specific entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapVerifyAdapterCapability(entityType, actionType, entityId, callback)</td>
    <td style="padding:15px">This call verifies the adapter can perform the provided action on the specific entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterEntityCache()</td>
    <td style="padding:15px">This call will update the entity cache.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Akamai_property_manager. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">bulkactivateasetofproperties(contractId, groupId, body, callback)</td>
    <td style="padding:15px">Bulk activate a set of properties</td>
    <td style="padding:15px">{base_path}/{version}/bulk/activations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listbulkActivatedproperties(contractId, groupId, bulkActivationId, callback)</td>
    <td style="padding:15px">List bulk-activated properties</td>
    <td style="padding:15px">{base_path}/{version}/bulk/activations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listactivations(contractId, groupId, propertyId, callback)</td>
    <td style="padding:15px">List activations</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/activations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createanewactivationordeactivation(contractId, groupId, propertyId, body, callback)</td>
    <td style="padding:15px">Create a new activation or deactivation</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/activations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkversionasetofproperties(contractId, groupId, body, callback)</td>
    <td style="padding:15px">Bulk version a set of properties</td>
    <td style="padding:15px">{base_path}/{version}/bulk/property-version-creations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listbulkVersionedproperties(contractId, groupId, bulkCreateId, callback)</td>
    <td style="padding:15px">List bulk-versioned properties</td>
    <td style="padding:15px">{base_path}/{version}/bulk/property-version-creations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkpatchasetofproperties(contractId, groupId, body, callback)</td>
    <td style="padding:15px">Bulk patch a set of properties</td>
    <td style="padding:15px">{base_path}/{version}/bulk/rules-patch-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listbulkPatchedproperties(contractId, groupId, bulkPatchId, callback)</td>
    <td style="padding:15px">List bulk-patched properties</td>
    <td style="padding:15px">{base_path}/{version}/bulk/rules-patch-requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulksearchasetofproperties(contractId, groupId, body, callback)</td>
    <td style="padding:15px">Bulk search a set of properties</td>
    <td style="padding:15px">{base_path}/{version}/bulk/rules-search-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listbulksearchresults(contractId, groupId, bulkSearchId, callback)</td>
    <td style="padding:15px">List bulk search results</td>
    <td style="padding:15px">{base_path}/{version}/bulk/rules-search-requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">synchronouslybulksearchasetofproperties(contractId, groupId, body, callback)</td>
    <td style="padding:15px">Synchronously bulk search a set of properties</td>
    <td style="padding:15px">{base_path}/{version}/bulk/rules-search-requests-synch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getclientsettings(callback)</td>
    <td style="padding:15px">Get client settings</td>
    <td style="padding:15px">{base_path}/{version}/client-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateclientsettings(body, callback)</td>
    <td style="padding:15px">Update client settings</td>
    <td style="padding:15px">{base_path}/{version}/client-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCPcodes(contractId, groupId, callback)</td>
    <td style="padding:15px">List CP codes</td>
    <td style="padding:15px">{base_path}/{version}/cpcodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createanewCPcode(contractId, groupId, body, callback)</td>
    <td style="padding:15px">Create a new CP code</td>
    <td style="padding:15px">{base_path}/{version}/cpcodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaCPcode(contractId, groupId, cpcodeId, callback)</td>
    <td style="padding:15px">Get a CP code</td>
    <td style="padding:15px">{base_path}/{version}/cpcodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteacustombehavior(behaviorId, callback)</td>
    <td style="padding:15px">Delete a custom behavior</td>
    <td style="padding:15px">{base_path}/{version}/custom-behaviors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getacustombehavior(behaviorId, callback)</td>
    <td style="padding:15px">Get a custom behavior</td>
    <td style="padding:15px">{base_path}/{version}/custom-behaviors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lockacustombehavior(lock, behaviorId, body, callback)</td>
    <td style="padding:15px">Lock a custom behavior</td>
    <td style="padding:15px">{base_path}/{version}/custom-behaviors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listcustombehaviors(callback)</td>
    <td style="padding:15px">List custom behaviors</td>
    <td style="padding:15px">{base_path}/{version}/custom-behaviors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createanewcustombehavior(body, callback)</td>
    <td style="padding:15px">Create a new custom behavior</td>
    <td style="padding:15px">{base_path}/{version}/custom-behaviors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteacustomoverride(overrideId, callback)</td>
    <td style="padding:15px">Delete a custom override</td>
    <td style="padding:15px">{base_path}/{version}/custom-overrides/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getacustomoverride(overrideId, callback)</td>
    <td style="padding:15px">Get a custom override</td>
    <td style="padding:15px">{base_path}/{version}/custom-overrides/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lockacustomoverride(lock, overrideId, body, callback)</td>
    <td style="padding:15px">Lock a custom override</td>
    <td style="padding:15px">{base_path}/{version}/custom-overrides/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listcustomoverrides(callback)</td>
    <td style="padding:15px">List custom overrides</td>
    <td style="padding:15px">{base_path}/{version}/custom-overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createanewcustomoverride(body, callback)</td>
    <td style="padding:15px">Create a new custom override</td>
    <td style="padding:15px">{base_path}/{version}/custom-overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listedgehostnames(contractId, groupId, options, callback)</td>
    <td style="padding:15px">List edge hostnames</td>
    <td style="padding:15px">{base_path}/{version}/edgehostnames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createanewedgehostname(contractId, groupId, options, body, callback)</td>
    <td style="padding:15px">Create a new edge hostname</td>
    <td style="padding:15px">{base_path}/{version}/edgehostnames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getanedgehostname(contractId, groupId, options, edgeHostnameId, callback)</td>
    <td style="padding:15px">Get an edge hostname</td>
    <td style="padding:15px">{base_path}/{version}/edgehostnames/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listproducts(contractId, callback)</td>
    <td style="padding:15px">List products</td>
    <td style="padding:15px">{base_path}/{version}/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listusecases(contractId, productId, callback)</td>
    <td style="padding:15px">List use cases</td>
    <td style="padding:15px">{base_path}/{version}/products/{pathv1}/mapping-use-cases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelapendingactivation(contractId, groupId, propertyId, activationId, callback)</td>
    <td style="padding:15px">Cancel a pending activation</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/activations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getanactivation(contractId, groupId, propertyId, activationId, callback)</td>
    <td style="padding:15px">Get an activation</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/activations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listapropertyShostnames(contractId, groupId, validateHostnames, propertyId, propertyVersion, callback)</td>
    <td style="padding:15px">List a property's hostnames</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions/{pathv2}/hostnames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateapropertyShostnames(contractId, groupId, validateHostnames, propertyId, propertyVersion, body, callback)</td>
    <td style="padding:15px">Update a property's hostnames</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions/{pathv2}/hostnames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaruletree(contractId, groupId, validateRules, validateMode, propertyId, propertyVersion, callback)</td>
    <td style="padding:15px">Get a rule tree</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions/{pathv2}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patcharuletree(contractId, groupId, validateRules, validateMode, dryRun, propertyId, propertyVersion, body, callback)</td>
    <td style="padding:15px">Patch a rule tree</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions/{pathv2}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatearuletree(contractId, groupId, validateRules, validateMode, dryRun, propertyId, propertyVersion, body, callback)</td>
    <td style="padding:15px">Update a rule tree</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions/{pathv2}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaversion(contractId, groupId, propertyId, propertyVersion, callback)</td>
    <td style="padding:15px">Get a version</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listavailablebehaviors(contractId, groupId, propertyId, propertyVersion, callback)</td>
    <td style="padding:15px">List available behaviors</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions/{pathv2}/available-behaviors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listavailablecriteria(contractId, groupId, propertyId, propertyVersion, callback)</td>
    <td style="padding:15px">List available criteria</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions/{pathv2}/available-criteria?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listpropertyversions(contractId, groupId, offset, limit, propertyId, callback)</td>
    <td style="padding:15px">List property versions</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createanewpropertyversion(contractId, groupId, propertyId, body, callback)</td>
    <td style="padding:15px">Create a new property version</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getthelatestversion(activatedOn, contractId, groupId, propertyId, callback)</td>
    <td style="padding:15px">Get the latest version</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}/versions/latest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeaproperty(contractId, groupId, propertyId, callback)</td>
    <td style="padding:15px">Remove a property</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaproperty(contractId, groupId, propertyId, callback)</td>
    <td style="padding:15px">Get a property</td>
    <td style="padding:15px">{base_path}/{version}/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listproperties(contractId, groupId, callback)</td>
    <td style="padding:15px">List properties</td>
    <td style="padding:15px">{base_path}/{version}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createorcloneaproperty(contractId, groupId, body, callback)</td>
    <td style="padding:15px">Create or clone a property</td>
    <td style="padding:15px">{base_path}/{version}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaruleformatSschema(productId, ruleFormat, callback)</td>
    <td style="padding:15px">Get a rule format's schema</td>
    <td style="padding:15px">{base_path}/{version}/schemas/products/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getarequestSschema(filename, callback)</td>
    <td style="padding:15px">Get a request's schema</td>
    <td style="padding:15px">{base_path}/{version}/schemas/request/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getbuilddetails(callback)</td>
    <td style="padding:15px">Get build details</td>
    <td style="padding:15px">{base_path}/{version}/build?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listcontracts(callback)</td>
    <td style="padding:15px">List contracts</td>
    <td style="padding:15px">{base_path}/{version}/contracts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdebugClientInfo(callback)</td>
    <td style="padding:15px">GET debug-client-info</td>
    <td style="padding:15px">{base_path}/{version}/debug/client-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listgroups(callback)</td>
    <td style="padding:15px">List groups</td>
    <td style="padding:15px">{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listruleformats(callback)</td>
    <td style="padding:15px">List rule formats</td>
    <td style="padding:15px">{base_path}/{version}/rule-formats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchproperties(body, callback)</td>
    <td style="padding:15px">Search properties</td>
    <td style="padding:15px">{base_path}/{version}/search/find-by-value?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateruleswithoutpersistence(contractId, groupId, body, callback)</td>
    <td style="padding:15px">Validate rules without persistence</td>
    <td style="padding:15px">{base_path}/{version}/validate/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
