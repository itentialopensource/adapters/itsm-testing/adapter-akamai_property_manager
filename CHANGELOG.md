
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:08PM

See merge request itentialopensource/adapters/adapter-akamai_property_manager!13

---

## 0.4.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-akamai_property_manager!11

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:16PM

See merge request itentialopensource/adapters/adapter-akamai_property_manager!10

---

## 0.4.2 [08-06-2024]

* Changes made at 2024.08.06_19:29PM

See merge request itentialopensource/adapters/adapter-akamai_property_manager!9

---

## 0.4.1 [08-05-2024]

* Changes made at 2024.08.05_18:20PM

See merge request itentialopensource/adapters/adapter-akamai_property_manager!8

---

## 0.4.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-akamai_property_manager!7

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_14:00PM

See merge request itentialopensource/adapters/itsm-testing/adapter-akamai_property_manager!6

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_15:02PM

See merge request itentialopensource/adapters/itsm-testing/adapter-akamai_property_manager!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:43PM

See merge request itentialopensource/adapters/itsm-testing/adapter-akamai_property_manager!4

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:07AM

See merge request itentialopensource/adapters/itsm-testing/adapter-akamai_property_manager!3

---

## 0.3.0 [12-30-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-akamai_property_manager!2

---

## 0.2.0 [06-09-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-akamai_property_manager!1

---
