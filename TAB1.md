# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Akamai_property_manager System. The API that was used to build the adapter for Akamai_property_manager is usually available in the report directory of this adapter. The adapter utilizes the Akamai_property_manager API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Akamai Property Manager adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Akamai Property Manager. 
With this adapter you have the ability to perform operations with Akamai Property Manager such as:

- Get Property
- Remove Property
- Get Rule
- Get Hostname
- Get Product

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
