# Akamai Property Manager

Vendor: Akamai
Homepage: https://www.akamai.com/

Product: Property Manager
Product Page: https://www.akamai.com/solutions/edge

## Introduction
We classify Akamai Property Manager into the ITSM or Service Management domain as Akamai Property Manager provides methods for managing and configuring the properties that determine how Akamai edge servers process requests, responses, and objects served over the Akamai platform.

## Why Integrate
The Akamai Property Manager adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Akamai Property Manager. 
With this adapter you have the ability to perform operations with Akamai Property Manager such as:

- Get Property
- Remove Property
- Get Rule
- Get Hostname
- Get Product

## Additional Product Documentation
[API documents for Akamai Property Manager](https://techdocs.akamai.com/property-mgr/reference/api)

[Akamai Propery Manager Tech Docs](https://techdocs.akamai.com/property-mgr/docs/welcome-prop-manager)